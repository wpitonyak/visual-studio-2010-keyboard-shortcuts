﻿// Type: EnvDTE.DTE
// Assembly: EnvDTE, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
// Assembly location: C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\PublicAssemblies\EnvDTE.dll

using System.Runtime.InteropServices;

namespace EnvDTE
{
    [CoClass(typeof (DTEClass))]
    [Guid("04A72314-32E9-48E2-9B87-A63603454F3E")]
    [ComImport]
    public interface DTE : _DTE
    {
    }
}
