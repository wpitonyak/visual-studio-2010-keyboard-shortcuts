﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using EnvDTE;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

namespace KeyboardShortcuts
{
    class Program
    {
        static readonly string emptyName = "(Empty Name)";

        static void Main(string[] args)
        {
            var showLess =         args.Contains("-less"); //only display Global and Text Editor scopes
            var removeDuplicates = args.Contains("-removeDuplicates");
            var removeEmpty =      args.Contains("-removeEmpty");

            DTE dte;

            try
            {
                dte = (DTE)Marshal.GetActiveObject("VisualStudio.DTE.10.0");
            }
            catch (Exception)
            {
                Console.WriteLine("Could not load EnvDTE. Make sure that Visual Studio 2010 is running.");
                throw;
            }

            Console.WriteLine("Reading Visual Studio key bindings.");
            //read the key bindings out of visual studio
            var commands = from Command command in dte.Commands
                           where !removeEmpty || command.Name.Length > 0
                           let bindings = (object[])command.Bindings
                           select new
                           {
                               CommandName = command.Name.Length > 0 ? command.Name : emptyName,
                               IsCurrentlyApplicable = command.IsAvailable,
                               Bindings = from string binding in bindings
                                          let bindingParts = binding.Split(new[] { "::" }, 2, StringSplitOptions.None)
                                          select new
                                          {
                                              Scope = bindingParts[0],
                                              KeyCombination = bindingParts[1]
                                          }
                           };

            var lines = Properties.Resources.Descriptions.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            Console.WriteLine("Mapping command descriptions.");
            //we can map these descriptions to command names
            var descriptions = lines.Where(l => l.Contains("\t")).Select(l => new
                                                     {
                                                         CommandName = l.Split('\t')[0],
                                                         Description = l.Split('\t')[1].Trim(new[] { '\"' }).Replace(". ", "\r\n\r\n"),
                                                     });

            Console.WriteLine("Polishing input.");
            //transform the keybindings into a scope hierarchy
            var scopes = from command in commands
                         from binding in command.Bindings
                         group new { command, binding } by binding.Scope into scopeShortcuts
                         orderby scopeShortcuts.Key
                         select new
                         {
                             ScopeName = scopeShortcuts.Key,
                             Shortcuts = from scopeShortcut in scopeShortcuts
                                         orderby scopeShortcut.command.CommandName
                                         let description = descriptions.FirstOrDefault(d => d.CommandName.Equals(scopeShortcut.command.CommandName))
                                         select new
                                         {
                                             CommandName = scopeShortcut.command.CommandName
                                                           .Replace(".", " > ")
                                                           .Replace("_", " > "),
                                             KeyCombination = scopeShortcut.binding.KeyCombination
                                                              .Replace("Left Arrow", "←")
                                                              .Replace("Right Arrow", "→")
                                                              .Replace("Up Arrow", "↑")
                                                              .Replace("Down Arrow", "↓")
                                                              .Replace("+", " + "),
                                             Description = description == null ? null : description.Description
                                         }
                         };

            if (showLess) //only show global and text editor scopes
            {
                Console.WriteLine("Showing only Global and Text Editor scopes.");
                scopes = scopes.Where(scope => new[] { "Global", "Text Editor" }.Contains(scope.ScopeName));
            }

            //remove duplicates
            if (removeDuplicates)
            {
                Console.WriteLine("Removing duplicate shortcuts.");

                //favor shortcuts with less keys for commands with multiple shortcuts,    
                scopes = from scope in scopes
                         select new
                                    {
                                        scope.ScopeName,
                                        Shortcuts = from shortcut in scope.Shortcuts
                                                    group shortcut by shortcut.CommandName into shortcuts
                                                    //which shortcut has the least amount of keys, determined by the number of spaces
                                                    let smallestShortcut = shortcuts.OrderBy(shortcut => shortcut.KeyCombination.Split(' ').Length).First()
                                                    select new
                                                               {
                                                                   CommandName = shortcuts.Key,
                                                                   smallestShortcut.KeyCombination,
                                                                   smallestShortcut.Description
                                                               }
                                    };

                //remove shortcuts from non-global scopes if they are contained within the global scope

                //split out global scope
                var globalScope = scopes.Where(scope => scope.ScopeName.Equals("Global"));
                var globalShortcuts = globalScope.First().Shortcuts;

                //split out non-global scopes
                scopes = from scope in scopes
                         where !scope.ScopeName.Equals("Global")
                         select new
                         {
                             scope.ScopeName,
                             Shortcuts = from shortcut in scope.Shortcuts
                                         //remove global shortcuts from non-global scopes
                                         where !globalShortcuts.Any(globalShortcut => globalShortcut.CommandName.Equals(shortcut.CommandName))
                                         select new
                                         {
                                             shortcut.CommandName,
                                             shortcut.KeyCombination,
                                             shortcut.Description
                                         }
                         };

                //done filtering, combine global and non-global
                scopes = scopes.Concat(globalScope);
            }

            var filename = String.Format("shortcuts {0}.html", DateTime.Now.ToString("M-d-yyyy"));

            Console.WriteLine("Building output html. This may take a minute.");
            using (var w = new StreamWriter(filename, false, Encoding.UTF8))
            {
                var top =
                    @"
                        <!DOCTYPE html>
                        <html>
                            <head>
                                <meta charset=""utf-8"" />
                                <title>{0}</title>
		                        <style type=""text/css"">
			                        body {{
				                        font-family: Arial;
			                        }}
			
                                    .scope {{
                                        font-size: .8em;
                                        color: #555;
                                        width: 100%;               
                                    }}

			                        .scope thead tr:first-child th {{
				                        background-color: rgb(180, 230, 235);
                                        border-radius: 6px 6px 0px 0px;
			                        }}
            
                                    .scope thead tr:nth-of-type(2) th {{
                                        background-color: rgb(210, 235, 235);
                                    }}           

                                    .scope thead th {{
                                        padding: 5px;
                                    }}
			
                                    .scope tbody [title] {{
                                        color: rgb(90, 120, 150);
                                    }}
                
			                        .scope tbody td {{
				                        padding-left: 10px;
				                        padding-right: 10px;
			                        }}

                                    .shortcut {{
                                        width: 25%;
                                    }}

                                    .scope tbody tr:nth-of-type(2n) td {{
                                        background-color: rgb(210, 235, 235);
                                    }}
		                        </style>
                            </head>
                            <body>
                                <h2>{0}</h2>
                    ";

                var tabletop =
                    @"
		                        <table class=""scope"" cellspacing=""0"">
			                        <thead>
				                        <tr>
					                        <th colspan=""2"">{0}</th>
				                        </tr>
				                        <tr>
					                        <th>Command</th>
					                        <th>Shortcut</th>
				                        </tr>
			                        </thead>
			                        <tbody>
                    ";

                var tablerow =
                    @"
				                        <tr>
					                        <td class=""command""{2}>{0}</td>
					                        <td class=""shortcut"">{1}</td>
				                        </tr>
                    ";

                var tablebottom =
                    @"
                    			    </tbody>
		                        </table>
                                <br /><br />
                    ";

                var bottom =
                    @"
                    	    </body>
                        </html>   
                    ";

                w.WriteLine(top, "Visual Studio Shortcuts");

                foreach (var scope in scopes)
                {
                    w.WriteLine(tabletop, scope.ScopeName);
                    foreach (var shortcut in scope.Shortcuts)
                    {
                        w.WriteLine(tablerow, shortcut.CommandName, shortcut.KeyCombination, shortcut.Description != null ? " title=\"" + shortcut.Description + "\"" : "");
                    }
                    w.WriteLine(tablebottom);
                }

                w.WriteLine(bottom);
            }

            Console.WriteLine("Finished. Opening html file.");
            new System.Diagnostics.Process { StartInfo = new ProcessStartInfo(filename) }.Start();
        }
    }
}
